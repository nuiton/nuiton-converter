package org.nuiton.converter;

/*
 * #%L
 * Nuiton Converter
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import junit.framework.TestCase;
import org.apache.commons.beanutils.Converter;

import java.util.Locale;

/**
 * Tests {@link LocaleConverter}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class LocaleConverterTest extends TestCase {

    String toConvert;

    Locale excepted;

    Converter converter;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        converter = new LocaleConverter();
    }

    public void testConvertFull() throws Exception {
        toConvert = "fr_FR";
        excepted = Locale.FRANCE;
        assertEquals(toConvert, excepted);

        toConvert = "fr_fr";
        assertEquals(toConvert, excepted);

        toConvert = "FR_fr";
        assertEquals(toConvert, excepted);

        toConvert = "FR_FR";
        assertEquals(toConvert, excepted);

        toConvert = "\n\tFr_fR  ";
        assertEquals(toConvert, excepted);

        toConvert = "en_GB";
        excepted = Locale.UK;
        assertEquals(toConvert, excepted);

        toConvert = "en_US";
        excepted = Locale.US;
        assertEquals(toConvert, excepted);

        //TODO Arch, we must also check coherence !
        toConvert = "fr_GB";
        excepted = new Locale("fr", "GB");
        assertEquals(toConvert, excepted);
    }

    public void testConvertMedium() throws Exception {
        toConvert = "fr";
        excepted = new Locale("fr");
        assertEquals(toConvert, excepted);

        toConvert = "fR";
        assertEquals(toConvert, excepted);

        toConvert = "FR";
        assertEquals(toConvert, excepted);

        toConvert = " fR \t";
        assertEquals(toConvert, excepted);

        toConvert = "en";
        excepted = new Locale("en");
        assertEquals(toConvert, excepted);

        toConvert = "es";
        excepted = new Locale("es");
        assertEquals(toConvert, excepted);

    }

    public void testConvertFailed() throws Exception {

        toConvert = null;
        assertConvertFailed(toConvert);

        toConvert = "";
        assertConvertFailed(toConvert);

        toConvert = "fr_";
        assertConvertFailed(toConvert);

        toConvert = "_FR";
        assertConvertFailed(toConvert);

    }

    protected void assertEquals(String toConvert, Locale expected) {
        Object result = converter.convert(Locale.class, toConvert);
        assertEquals(expected, result);
    }

    protected void assertConvertFailed(String toConvert) {
        try {
            converter.convert(Locale.class, toConvert);
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }

    }
}
