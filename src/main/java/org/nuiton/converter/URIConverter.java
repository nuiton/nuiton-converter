package org.nuiton.converter;

/*
 * #%L
 * Nuiton Converter
 * %%
 * Copyright (C) 2014 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.logging.Log;

import java.net.URI;
import java.net.URISyntaxException;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * To convert {@link URI} from and to {@link String}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class URIConverter implements NuitonConverter<URI> {

    /** Logger. */
    private static final  Log LOGGER = getLog(URIConverter.class);

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new ConversionException(
                    String.format("No value specified for converter %s", this));
        }
        if (isEnabled(aClass)) {
            Object result;
            if (isEnabled(value.getClass())) {
                result = value;
                return aClass.cast(result);
            }
            if (value instanceof String) {
                result = valueOf((String) value);
                return aClass.cast(result);
            }
        }
        throw new ConversionException(
                String.format("no convertor found for type %2$s and objet '%1$s'", aClass.getName(), value));
    }

    protected URI valueOf(String value) {
        try {
            URI result;
            result = new URI(value);
            return result;
        } catch (URISyntaxException e) {
            throw new ConversionException(
                    String.format("a problem occurs while converting value '%s' with url convertor %s", value, this), e);
        }
    }

    public URIConverter() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("init uri converter " + this);
        }
    }

    protected boolean isEnabled(Class<?> aClass) {
        return URI.class.equals(aClass);
    }

    @Override
    public Class<URI> getType() {
        return URI.class;
    }
}
