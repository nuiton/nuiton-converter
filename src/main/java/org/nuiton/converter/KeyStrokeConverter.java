package org.nuiton.converter;

/*
 * #%L
 * Nuiton Converter
 * %%
 * Copyright (C) 2014 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.logging.Log;

import javax.swing.KeyStroke;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * To convert {@link KeyStroke} from and to {@link String}.
 *
 * @author Sylvain Letellier - letellier@codelutin.com
 * @since 1.0
 */
public class KeyStrokeConverter implements NuitonConverter<KeyStroke> {

    private static final Log LOGGER = getLog(KeyStrokeConverter.class);

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new ConversionException(
                    String.format("No value specified for converter %s", this));
        }
        if (isEnabled(aClass)) {
            Object result;
            if (isEnabled(value.getClass())) {
                result = value;
                return aClass.cast(result);
            }
            if (value instanceof String) {
                result = KeyStroke.getKeyStroke((String) value);
                return aClass.cast(result);
            }
        }
        throw new ConversionException(
                String.format("no convertor found for type %2$s and objet '%1$s'", aClass.getName(), value));
    }

    protected boolean isEnabled(Class<?> aClass) {
        return KeyStroke.class.equals(aClass);
    }

    @Override
    public Class<KeyStroke> getType() {
        return KeyStroke.class;
    }

    public KeyStrokeConverter() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("init keystroke converter " + this);
        }
    }

}
