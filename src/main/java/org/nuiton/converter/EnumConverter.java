package org.nuiton.converter;

/*
 * #%L
 * Nuiton Converter
 * %%
 * Copyright (C) 2014 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.logging.Log;

import java.util.EnumSet;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * To convert {@link Enum} type-safe (exact type is {@link #enumType}
 * from and to {@link String}.
 * <p>
 * It is possible also to convert the enum object from his ordinal using method {@link #convertFromOrdinal(Class, Object)}.
 * <p>
 * you can also define a default value while trying to convert {@code null} value.
 * <p>
 * To register a new such converter use methods {@link ConverterUtil#registerEnumConverter(Class)},
 * or {@link ConverterUtil#registerEnumConverter(Class, Object)} .
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see Enum
 * @see Enum#ordinal()
 * @since 1.0
 */
public class EnumConverter<E extends Enum> implements NuitonConverter<E> {

    /** Logger. */
    private static final Log LOGGER = getLog(EnumConverter.class);

    /**
     * Default value to use if try to convert {@code null} value and flag {@link #useDefault} is on.
     */
    protected Object defaultValue;

    /**
     * flag to use or not default value.
     */
    protected boolean useDefault;

    /**
     * Enum type-safe.
     */
    protected Class<E> enumType;

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            if (useDefault) {
                return aClass.cast(defaultValue);
            }
            throw new ConversionException(String.format("No value specified for converter %s", this));
        }
        if (isEnabled(aClass, enumType)) {
            Object result;
            if (isEnabled(value.getClass(), enumType)) {
                result = value;
                return aClass.cast(result);
            }
            if (value instanceof String) {
                try {
                    result = valueOf(aClass, value);
                } catch (IllegalArgumentException e) {
                    // try an ordinal conversion
                    result = convertFromOrdinal(aClass, value);
                }
                return aClass.cast(result);
            }
            if (value instanceof Integer) {
                // try a ordinal conversion
                result = convertFromOrdinal(aClass, value);
                return aClass.cast(result);
            }
        }
        throw new ConversionException(
                String.format("no convertor found for type %2$s and objet '%1$s'", aClass.getName(), value));
    }

    public EnumConverter(Class<E> enumType, Object defaultValue) {
        this.enumType = enumType;
        this.defaultValue = defaultValue;
        useDefault = defaultValue != null;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(toString() + '<' + enumType + '>');
        }
    }

    public EnumConverter(Class<E> enumType) {
        this(enumType, null);
    }

    @Override
    public Class<E> getType() {
        return enumType;
    }

    protected static boolean isEnabled(Class<?> aClass, Class<?> enumType) {
        return aClass != null && aClass.isEnum() && aClass.equals(enumType);
    }

    protected Object convertFromOrdinal(Class<?> aClass, Object value) {
        Object result = null;
        try {
            int ordinal = Integer.valueOf(value + "");
            EnumSet<?> vals = allOf(aClass);
            if (ordinal > -1 && ordinal < vals.size()) {
                for (Enum<?> val : vals) {
                    if (val.ordinal() == ordinal) {
                        result = val;
                        break;
                    }
                }
            }
        } catch (NumberFormatException e1) {
            // quiet conversion
            result = null;
        }
        return result;
    }

    protected Object valueOf(Class<?> aClass, Object value) {
        Object result;
        result = Enum.valueOf((Class) aClass, (String) value);
        return result;
    }

    protected EnumSet<?> allOf(Class<?> aClass) {
        EnumSet<?> vals;
        vals = EnumSet.allOf((Class) aClass);
        return vals;
    }


}
