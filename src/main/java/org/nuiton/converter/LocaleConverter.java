package org.nuiton.converter;

/*
 * #%L
 * Nuiton Converter
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.logging.Log;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * To convert {@link Locale} from and to {@link String}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class LocaleConverter implements NuitonConverter<Locale> {

    private static final Pattern FULL_SCOPE_PATTERN =
            Pattern.compile("([a-zA-Z]{2})_([a-zA-Z]{2})");

    private static final Pattern MEDIUM_SCOPE_PATTERN =
            Pattern.compile("([a-zA-Z]{2})");

    /** Logger. */
    private static final Log LOGGER = getLog(LocaleConverter.class);

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new ConversionException("can not convert null value in " +
                                          this + " convertor");
        }
        if (isEnabled(aClass)) {
            Object result;
            if (isEnabled(value.getClass())) {
                result = value;
                return aClass.cast(result);
            }
            if (value instanceof String) {
                result = valueOf(((String) value).trim());
                return aClass.cast(result);
            }
        }
        throw new ConversionException(
                "could not find a convertor for type " + aClass.getName() +
                " and value : " + value);
    }

    public Locale valueOf(String value) {

        Locale result = convertFullScope(value);

        if (result == null) {
            result = convertMediumScope(value);
        }

        if (result == null) {
            throw new ConversionException("could not convert locale " + value);
        }

        return result;

    }

    private Locale convertFullScope(String value) {
        Matcher m = FULL_SCOPE_PATTERN.matcher(value);
        if (m.matches()) {
            // found a full scope pattern (language + country)
            String language = m.group(1).toLowerCase();
            String country = m.group(2).toUpperCase();

            return new Locale(language, country);
        }
        return null;
    }

    private Locale convertMediumScope(String value) {
        Matcher m = MEDIUM_SCOPE_PATTERN.matcher(value);
        if (m.matches()) {

            // found a medium scope pattern (only language)
            String language = m.group(1).toLowerCase();

            return new Locale(language);
        }
        return null;
    }

    public LocaleConverter() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("init locale converter : " + this);
        }
    }

    protected boolean isEnabled(Class<?> aClass) {
        return Locale.class.equals(aClass);
    }

    @Override
    public Class<Locale> getType() {
        return Locale.class;
    }

}
