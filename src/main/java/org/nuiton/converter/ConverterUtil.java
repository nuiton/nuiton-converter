package org.nuiton.converter;

/*
 * #%L
 * Nuiton Converter
 * %%
 * Copyright (C) 2014 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ServiceLoader;

/**
 * Useful class to get / register converter from {@link ConvertUtils}
 * or trying to get it from this package.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ConverterUtil {

    /** Logger. */
    private static final Log LOGGER = LogFactory.getLog(ConverterUtil.class);

    /**
     * Default package where to find the converter.
     */
    protected static final String CONVERTER_PACKAGE = "org.nuiton.converter";

    /**
     * State of default converters registering. If set to {@code false}, then the will invoke the method {@link #initConverters()}
     * the vry first time while calling methods {@link #getConverter(Class)} or {@link #convert(Class, Object)}.
     * <p>
     * Method {@link #deregister()} will set back the flag value to {@code false}.
     */
    private static Boolean wasInit = Boolean.FALSE;

    /**
     * Try to find amatching converter for the given {@code type}.
     * <p>
     * If required, init defaults converters (see {@link #initConverters()}).
     * <p>
     * If not found and type is an enum, then will register a enum type converter.
     * <p>
     * If still not found, try to register a converter having the {@link NuitonConverter} contract named {@code typeConverter}
     * in the current package ({@link #CONVERTER_PACKAGE}).
     * <p>
     * For example with type {@code MyType}, will try to find the converter {@code org.nuiton.converter.MyTypeConverter}.
     *
     * @param <T>  type to convert
     * @param type type to convert
     * @return the found converter, or {@code null} if not found
     */
    public static <T> Converter getConverter(Class<T> type) {
        if (!wasInit) {
            initConverters();
        }
        Converter converter = ConvertUtils.lookup(type);
        if (converter != null) {
            return converter;
        }
        if (type.isEnum()) {
            registerEnumConverter((Class) type);
            return ConvertUtils.lookup(type);
        }
        // try in the converter package
        try {
            registerConverter0(type);
            converter = ConvertUtils.lookup(type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return converter;
    }

    /**
     * Try to convert a value.
     * <p>
     * Will first try to find a matching converter, if not found return then {@code null}.
     * <p>
     * Note: will init default converters or matching data type converter if required.
     *
     * @param <T>       type of data to convert
     * @param type      type of data to convert
     * @param toConvert data to convert
     * @return the converted data or {@code null} if no converter found for the type of data.
     */
    public static <T> T convert(Class<T> type, Object toConvert) {
        if (!wasInit) {
            initConverters();
        }
        T result = null;
        Converter converter = getConverter(type);
        if (converter != null) {
            result = converter.convert(type, toConvert);
        }
        return result;
    }

    public static void registerConverter(Class<?> type)
            throws IllegalAccessException,
            InstantiationException,
            ClassNotFoundException {
        if (ConvertUtils.lookup(type) == null) {
            registerConverter0(type);
        }
    }

    protected static void registerConverter0(Class<?> type)
            throws IllegalAccessException,
            InstantiationException,
            ClassNotFoundException {
        Class<?> aClass = Class.forName(
                CONVERTER_PACKAGE + "." + type.getSimpleName() + "Converter");
        Converter converter = (Converter) aClass.newInstance();
        LOGGER.info("for type : " + type + " : " + converter);
        ConvertUtils.register(converter, type);
    }

    /**
     * Register a new converter for a given enum type,
     * using the given default value.
     *
     * @param type         enum type to convert
     * @param defaultValue default value to use.
     * @see EnumConverter
     */
    public static <E extends Enum> void registerEnumConverter(Class<E> type,
                                                              Object defaultValue) {
        if (EnumConverter.isEnabled(type, type) && ConvertUtils.lookup(type) == null) {
            Converter converter = new EnumConverter<E>(type, defaultValue);
            LOGGER.info("for type : " + type + " : " + converter);
            ConvertUtils.register(converter, type);
        }
    }

    /**
     * Register a new converter for a given enum type,
     * not using any default value.
     *
     * @param type enum type to convert
     * @see EnumConverter
     */
    public static <E extends Enum> void registerEnumConverter(Class<E> type) {
        registerEnumConverter(type, null);
    }

    /**
     * Converts a array of char to an array of bytes.
     * <p>
     * TODO Find out who uses this ?
     *
     * @param chars array of characters to converter
     * @return the array of bytes
     */
    public static byte[] convert(char[] chars) {
        byte[] bytes = new byte[chars.length];
        for (int i = 0; i < chars.length; i++) {
            bytes[i] = (byte) (chars[i] & 0xff);
        }
        return bytes;
    }

    /**
     * To remove any registered converter.
     * <p>
     * At the next call to method {@link #convert(Class, Object)} or {@link #getConverter(Class)}, default converters
     * will be registred again.
     */
    public static synchronized void deregister() {
        ConvertUtils.deregister();
        wasInit = false;
    }

    /**
     * To init default converters (using the service loader mecanism on contract {@link NuitonConverter}).
     * <p>
     * If was already init, then will do nothing.
     * <p>
     * Note: the {@link #getConverter(Class)} or {@link #convert(Class, Object)} methods always invoke this method if
     * the class was not initialized (see the {@link #wasInit} internal flag).
     */
    public static synchronized void initConverters() {
        if (wasInit != null && wasInit) {
            return;
        }
        ServiceLoader<NuitonConverter> converters = ServiceLoader.load(NuitonConverter.class);
        for (NuitonConverter converter : converters) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("discovered converter " + converter);
            }
            Class<?> type = converter.getType();
            LOGGER.info("register converter " + converter);
            ConvertUtils.register(converter, type);
        }
        wasInit = true;
    }

    protected ConverterUtil() {
        // avoid to instanciate this utility class
    }
}
