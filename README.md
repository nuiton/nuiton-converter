Abstract
--------

Nuiton Converter is a thin layer at the top of the commons-beanutils Converter API.

SCM
---

The project uses git.

To get it:

git clone https://git.nuiton.org/nuiton-converter.git

To commit to the project you must be a member of the project on the nuiton.org forge.

See https://forge.nuiton.org/projects/nuiton-converter

Build
-----

To build project just do a mvn clean install.

Release
-------

The project use codelutinpom, to make a release see documentation:
https://forge.nuiton.org/projects/pom/wiki/Comment_faire_une_release_d'un_projet_qui_utilise_pom